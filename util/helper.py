import math
import psutil
from dataclasses import dataclass, field
from typing import List

import cupy as cp
import numba as nb
import numpy as np
import pandas as pd
import seaborn as sns
from dask import array as da
from scipy.signal import windows


bytes_per_kb = 2 ** 10
bytes_per_mb = 2 ** 10 * bytes_per_kb
bytes_per_gb = 2 ** 10 * bytes_per_mb


@dataclass
class Config:
    cpu_count: int
    cpu_freq: float
    ram_mb: int
    workers: int = 2
    memoffset: int = 0

    @property
    def threads(self) -> int:
        return max(1, (self.cpu_count - 1) // self.workers)

    @property
    def ram_per_worker_gb(self) -> int:
        return self.ram_mb // self.workers // bytes_per_kb + self.memoffset

    @property
    def mlim(self) -> str:
        return f"{self.ram_per_worker_gb}GB"


config = Config(
    cpu_count=psutil.cpu_count(),
    cpu_freq=psutil.cpu_freq().current,
    ram_mb=psutil.virtual_memory().total // bytes_per_mb,
    memoffset=90,  # to avoid worker limit warnings
)


title = lambda f: f"\nConvolutions\nframesize = {str(f)}\n"


def padlr(pad: float, shift: int = 0):
    return (math.floor(pad) + shift, math.ceil(pad) - shift)


def realtime(n: int, m: int, fs: int = 48_000):
    return n * m / fs


big_chunk, sml_chunk = 2 ** 11, 2 ** 7


def square_and_hann_1d(s: int, fname: str):
    win = windows.hann(s // 4)
    sig = np.repeat([0.0, 1.0, 0.0], s)
    if "cupy" in fname or "gpu" in fname:
        return cp.asarray(sig, dtype="float32"), cp.asarray(win, dtype="float32")
    elif "numba" in fname:
        return np.pad(sig, padlr(win.size / 2), "constant"), win
    elif "python" in fname:
        return np.pad(sig, padlr(win.size / 2), "constant").tolist(), win.tolist()
    return sig, win


def square_and_hann(
    n: int, fname: str, framesize: int = 2 ** 12, ptn=[0.0, 1.0, 1.0, 0.0]
):
    s = framesize
    win = np.repeat(windows.hann(s), n).reshape(s, n).T.astype(np.float32)
    sig = np.repeat(np.repeat(ptn, s), n).reshape(s * len(ptn), n).T.astype(np.float32)
    if "cupy" in fname or "gpu" in fname:
        return cp.asarray(sig, dtype="float16"), cp.asarray(win, dtype="float16")
    if "numba" in fname:
        return np.pad(sig, ((0, 0), padlr((s - 1) / 2)), "constant"), win
    if "dask" in fname:
        chunk = max(2 ** 6, sig.shape[0] // 2 ** 4)
        return (
            da.from_array(sig, chunks=(chunk, sig.shape[1])).persist(),
            da.from_array(win, chunks=(chunk, win.shape[1])).persist(),
        )
    if "python" in fname:
        return (
            np.pad(sig, ((0, 0), padlr((s - 1) / 2)), "constant").tolist(),
            win.tolist(),
        )
    return sig, win


def postprocess_df(df, t):
    for k, v in t.genkwargs.items():
        df[k] = v
    df.time = df.apply(
        lambda x: realtime(2 ** x.n, x.framesize) if "realtime" in x.func else x.time,
        axis=1,
    )
    df = t.process_results(df)
    df.func = df.func.apply(lambda x: x.replace("convolve_", ""))
    df["domain"] = df.func.apply(
        lambda f: "n/a" if "realtime" in f else "freq" if "fft" in f else "time"
    )
    df["device_domain"] = df.apply(
        lambda x: "n/a" if "realtime" in x.func else x.device + "_" + x.domain, axis=1
    )
    df["samples"] = df.framesize * 2 ** df.n
    return df


fixed_types = {8: np.int8, 16: np.int16, 32: np.int32}

n = 32  # bits
eps = 1 / (2 ** (n + 1))
Amax = lambda n: 2 ** (n - 1) - 2  # to allow room to dither without overflow
cumavg = lambda a: a.cumsum(axis=-1) / np.arange(1, a.shape[-1] + 1)
mae = (
    lambda a, b: np.abs(a - b)
    .mean(axis=0)
    .round()
    .astype(type(a.flatten()[0]))
    .tolist()
)
rmse = lambda a, b: np.sqrt(cumavg(np.square(a - (b or 0.0))))
dbfs = lambda a, n: 20 * np.log10(a / 2 ** n + eps)
dither = lambda a, n: a + np.random.randint(0, 2, a.shape, dtype=fixed_types[n])
to_fixed_point = (
    lambda a, n: (Amax(n) * np.clip(a, -1.0, 1.0)).round().astype(fixed_types[n])
)


def compute_error(ref: np.ndarray, other: List[np.ndarray], funcs: List[str]):
    e: List[float] = []
    r: List[float] = []
    rfixed: List[float] = []
    size = range(ref.shape[1])
    ref_fixed = to_fixed_point(ref, n)
    for o in other:
        o_np = cp.asnumpy(o) if isinstance(o, cp.ndarray) else np.asarray(o)
        o_fixed = dither(to_fixed_point(o_np, n), n)
        r += o_np[0, :].tolist()
        rfixed += o_fixed[0, :].tolist()
        e += mae(ref_fixed, o_fixed)
    sample = [i for _ in range(len(other)) for i in size]
    func = [f for f in funcs for _ in size]
    df = pd.DataFrame(
        {"mae": e, "t": sample, "func": func, "result": r, "result_fixed": rfixed}
    )
    df["MAE (dBFS)"] = dbfs(df.mae, n)
    df["RMSE (dBFS)"] = df.groupby("func").mae.transform(
        lambda x: dbfs(rmse(x.values, None), n)
    )

    dft = (
        df.set_index(["t", "func", "result", "result_fixed"])
        .rename(columns={"mae": "MAE"})
        .stack()
        .reset_index()
        .rename(columns={"level_4": "type", 0: "error"})
    )

    return dft, df


sint = lambda x: f"{int(round(x)):,}"


def summary_table(df: pd.DataFrame, fs: int) -> pd.DataFrame:
    f, rt, t = "framesize", "realtime", "time (sec)"
    grp = [f, "device", "domain", "func"]
    time = [c for c in df.columns if "(sec)" in c or "n" == c]
    gdf = (
        df[(df.time != np.inf) & (df.func != rt)]
        .groupby(grp)[time]
        .max()
        .sort_values(by=[f, "n"])
    ).reset_index(f)
    gdf[rt] = gdf.apply(lambda x: x[f] / fs * 2 ** x.n, axis=1)
    gdf[f"factor of {rt}"] = gdf.apply(lambda x: f"{round(x[rt] / x[t], 1):,}x", axis=1)

    # format
    gdf[rt] = gdf[rt].apply(lambda x: sint(x))
    gdf[f] = gdf[f].apply(lambda x: sint(x))
    gdf.n = gdf.n.apply(lambda x: sint(x))
    return (
        gdf.set_index([f, gdf.index])
        .sort_index(axis=1, ascending=False)
        .rename(columns={"n": "2^n", rt: f"{rt} (sec)"})
        .round(1)
    )


pad = "   |   "
fmt_title = lambda g: [
    ax.set_title(ax.get_title().split("|")[-1].replace("type = ", ""))
    for axes in g.axes
    for ax in axes
]
fmt_ylabel = lambda g, n, val: [ax.set_ylabel(val) for ax in g.axes[:, n]]
fmt_ytick = lambda g, n, f: [ax.set_yticklabels(f(ax)) for ax in g.axes[:, n]]
fmt_pct = lambda ax: [f"{y / Amax(n):.5%}" for y in ax.get_yticks()]
fmt_int = lambda ax: [f"{y:,g}" for y in ax.get_yticks()]
fmt_all = lambda g: (
    fmt_title(g),
    fmt_ylabel(g, 0, "Fixed Point"),
    fmt_ylabel(g, 1, "dBFS"),
    fmt_ytick(g, 0, fmt_int),
)
