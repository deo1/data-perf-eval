import math
from dataclasses import dataclass, field
from collections import defaultdict as dd
from timeit import repeat
from typing import Any, Callable, DefaultDict as DD, Dict, List, Tuple

import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

pd.set_option("display.max_columns", 50)
pd.set_option("max_colwidth", 100)
pd.set_option("display.width", 250)


n2pow = lambda n: f"2^{int(math.log2(n))}"
pow2n = lambda n: int(n.split("^")[0]) ** int(n.split("^")[1])
order_n = lambda n: f"2^{n // 10 * 10:0g}"
palette = lambda df, c: sns.color_palette("hls", df[c].nunique(), desat=0.95)
device = (
    lambda fn: "gpu"
    if "gpu" in fn or "cupy" in fn
    else "purepy"
    if "python" in fn
    else "cpu"
)


def logrange(beg: int, end: int, num: int = 0, ratio: int = 1):
    beg, end = math.log2(beg), math.log2(end)  # type: ignore
    num = ratio * (end - beg) + 1 if num == 0 else num
    r = np.logspace(beg, end, num=num, base=2.0)
    return r.round().astype(int)


@dataclass
class Timer:
    funcs: List[Callable[[Any], Any]]
    genargs: Callable[[int, str], Any]
    repeat: int = 5
    number: int = 1
    beg: int = 7
    end: int = 25
    stop_th: float = 2.0
    unit: str = "samples"
    genkwargs: Dict[str, Any] = field(default_factory=dict)

    def rargs(self, glb):
        return {"repeat": self.repeat, "number": self.number, "globals": glb}

    def update_results(self, r, fn, n, beg, i, t) -> DD[str, List[Any]]:
        rr = range(self.repeat)
        r["iter"] += list(rr)
        r["n"] += [beg + i for _ in rr]
        r[self.unit] += [f"2^{beg + i}" for _ in rr]
        r["func"] += [fn for _ in rr]
        r["device"] += [device(fn) for _ in rr]
        r["order"] += [order_n(beg + i) for _ in rr]
        r["time"] += t if t else [np.inf for _ in rr]

        return r

    @staticmethod
    def process_results(df: pd.DataFrame) -> pd.DataFrame:
        df["time (ms)"] = 1_000 * df.time
        df["time (log2 ms)"] = np.log2(df["time (ms)"])
        df["time (log2 ms)"] += np.abs(df["time (log2 ms)"].min())
        df["time (log2 sec)"] = np.log2(df["time"])
        df["time (log2 sec)"] += np.abs(df["time (log2 sec)"].min())
        df["time (sec)"] = df.time.round(3)

        return df

    def timeit(self, n, fn, glb, stop) -> List[float]:
        glb.update({"targs": self.genargs(n, fn, **self.genkwargs)})  # type: ignore
        try:
            return [] if stop[fn] else repeat(f"{fn}(*targs)", **self.rargs(glb))
        except Exception as e:
            print(e)
            return []

    def run(self, glb) -> pd.DataFrame:
        beg, end = self.beg, self.end
        r, stop = dd(list), dd(bool)  # type: ignore

        # run all functions once to ensure compiled
        for f in self.funcs:
            t = self.timeit(2 ** beg, f.__name__, glb, stop)

        # benchmark functions
        for i, n in enumerate(tqdm(logrange(2 ** beg, 2 ** end))):
            for f in self.funcs:
                fn = f.__name__
                t = self.timeit(n, fn, glb, stop)
                stop[fn] = True if (t and min(t) > self.stop_th) else stop[fn]
                r = self.update_results(r, fn, n, beg, i, t)

        return self.process_results(pd.DataFrame(r))

    def plot_lin(
        self,
        df,
        hue="func",
        y="time (sec)",
        size=(15, 8),
        title: str = "",
        xs="linear",
        ys="linear",
        *args,
        **kwargs,
    ):
        kwargs.setdefault("x", self.unit)
        kwargs.setdefault("linewidth", 2)

        title = title or self.funcs[0].__name__.split("_")[0].title()
        ax = sns.lineplot(y=y, data=df, hue=hue, sort=False, **kwargs)
        [tick.set_rotation(45) for tick in ax.get_xticklabels()]
        if xs != "linear":
            ax.set_xscale(xs)
        if ys != "linear":
            ax.set_yscale(ys)
        ax.figure.set_size_inches(size)
        ax.set_title(title)

        return ax

    def plot_facet(
        self,
        df,
        hue="func",
        col="order",
        row="device",
        y="time (sec)",
        xs="linear",
        ys="linear",
        *args,
        **kwargs,
    ):
        kwargs.setdefault("x", self.unit)
        kwargs.setdefault("sort", False)
        kwargs.setdefault("height", 3.5)
        kwargs.setdefault("aspect", 1.7)
        kwargs.setdefault("linewidth", 2)
        kwargs.setdefault("facet_kws", {"sharey": False, "sharex": False})

        g = sns.relplot(y=y, data=df, kind="line", hue=hue, col=col, row=row, **kwargs)
        [ax.set_xscale(xs) for ax in g.axes if hasattr(ax, "set_xscale")]
        [ax.set_yscale(ys) for ax in g.axes if hasattr(ax, "set_yscale")]

        return g
